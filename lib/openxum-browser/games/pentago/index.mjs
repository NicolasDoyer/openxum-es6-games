"use strict";

import Pentago from '../../../openxum-core/games/pentago/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
  Gui: Gui,
  Manager: Manager,
  Settings: {
    ai: {
      mcts: AI.RandomPlayer // TODO: MCTSPlayer
    },
    colors: {
      first: Pentago.Color.BLACK,
      init: Pentago.Color.BLACK,
      list: [
        {key: Pentago.Color.BLACK, value: 'black'},
        {key: Pentago.Color.WHITE, value: 'white'}
      ]
    },
    modes: {
      init: Pentago.GameType.STANDARD,
      list: [
        {key: Pentago.GameType.STANDARD, value: 'standard'}
      ]
    },
    opponent_color(color) {
      return color === Pentago.Color.BLACK ? Pentago.Color.WHITE : Pentago.Color.BLACK;
    },
    types: {
      init: 'ai',
      list: [
        {key: 'gui', value: 'GUI'},
        {key: 'ai', value: 'AI'},
        {key: 'online', value: 'Online'},
        {key: 'offline', value: 'Offline'}
      ]
    }
  }
};